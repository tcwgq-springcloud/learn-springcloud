package com.tcwgq.scconfigclient2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScConfigClient2Application {

    public static void main(String[] args) {
        SpringApplication.run(ScConfigClient2Application.class, args);
    }

}
