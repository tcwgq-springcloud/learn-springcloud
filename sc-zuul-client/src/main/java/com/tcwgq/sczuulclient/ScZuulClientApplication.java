package com.tcwgq.sczuulclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ScZuulClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScZuulClientApplication.class, args);
    }

}
