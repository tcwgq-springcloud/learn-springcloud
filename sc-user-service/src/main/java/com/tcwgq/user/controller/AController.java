package com.tcwgq.user.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/a")
public class AController {
    @RequestMapping("/getA")
    public String getA(){
       return "A";
    }
}