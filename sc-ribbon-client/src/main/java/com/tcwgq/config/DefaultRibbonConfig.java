package com.tcwgq.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DefaultRibbonConfig {
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        // 默认使用轮询算法
        return new RestTemplate();

    }

    /*@Bean
    public IRule iRule() {
        return new BestAvailableRule();
    }

    @Bean
    public IPing iPing() {
        return new PingUrl();
    }
*/
    /*@Bean
    public ServerList<Server> serverServerList(IClientConfig config) {
        return new RibbonClientDefaultConfiguration.BazServiceList(config);
    }

    @Bean
    public ZonePreferenceServerListFilter serverListFilter() {
        ZonePreferenceServerListFilter filter = new ZonePreferenceServerListFilter();
        filter.setZone("myZone");
        return filter;
    }*/

}
