package com.tcwgq.sceurekaclient2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ScEurekaClient2Application {
    public static void main(String[] args) {
        SpringApplication.run(ScEurekaClient2Application.class, args);
    }

}
