package com.tcwgq.sceurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ScEurekaClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScEurekaClientApplication.class, args);
    }

}
